import java.util.Scanner;

public class nilai {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String jawaban_nama;

        System.out.println("Masukkan Nama Mahasiswa : ");
        jawaban_nama = input.nextLine();


        double jawaban_nilai_asli;

        System.out.println("Masukkan Nilai Asli : ");
        jawaban_nilai_asli = input.nextDouble();


        int jawaban_durasi;

        System.out.println("Masukkan durasi : ");
        jawaban_durasi = input.nextInt();

        double nilai_akhir;


        if(jawaban_durasi<60){
             nilai_akhir = 1.2 * jawaban_nilai_asli;
        }
        else if (60<= jawaban_durasi && jawaban_durasi <=70){
            nilai_akhir = jawaban_nilai_asli;
        }
        else if (70 < jawaban_durasi && jawaban_durasi < 90){
            nilai_akhir = 0.75 * jawaban_nilai_asli;
        }
        else if (90 <= jawaban_durasi && jawaban_durasi <= 100){
            nilai_akhir = 0.5 * jawaban_nilai_asli;
        }
        else{
            nilai_akhir = 0.2 * jawaban_nilai_asli;
        }

        System.out.println(jawaban_nama + " mendapatkan nilai akhir " + nilai_akhir);
    }
}
